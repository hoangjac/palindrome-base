package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean result = Palindrome.isPalindrome("racecar");
		assertTrue("The string is not a palindrome", result);
	}

	@Test
	public void testIsPalindromeException( ) {
		boolean result = Palindrome.isPalindrome("apple");
		assertFalse("The string is not a palindrome", result);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean result = Palindrome.isPalindrome("wow");
		assertTrue("The string is not a palindrome", result);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean result = Palindrome.isPalindrome("wo");
		assertFalse("The string is not a palindrome", result);
	}	
	
}
